package ru.infraymer.trezvoeradio

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.media.app.NotificationCompat.MediaStyle
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util


class PlayerActivity : AppCompatActivity(), Player.EventListener {

    val URL = "http://eu3.radioboss.fm:8259/stream?1480499964434"

    private var mStateBuilder: PlaybackStateCompat.Builder? = null
    private var mNotificationManager: NotificationManager? = null
    private var mPlayerView: SimpleExoPlayerView? = null
    private var token: MediaSessionCompat.Token? = null
    private var mExoPlayer: SimpleExoPlayer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        setupView()
    }

    private fun setupView() {
        mPlayerView = findViewById<SimpleExoPlayerView>(R.id.playerView)
        mPlayerView!!.defaultArtwork = BitmapFactory.decodeResource(resources, R.drawable.play_me_logo)

        mMediaSession = MediaSessionCompat(this, this.javaClass.simpleName)
        mMediaSession!!.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)

        // Do not let MediaButtons restart the player when the app is not visible.
        mMediaSession!!.setMediaButtonReceiver(null)

        mStateBuilder = PlaybackStateCompat.Builder().setActions(
            PlaybackStateCompat.ACTION_PLAY or
                PlaybackStateCompat.ACTION_PAUSE or
                PlaybackStateCompat.ACTION_PLAY_PAUSE or
                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)

        mMediaSession!!.setPlaybackState(mStateBuilder!!.build())
        mMediaSession!!.setCallback(MySessionCallback())
        mMediaSession!!.isActive = true

        initializePlayer(Uri.parse(URL))
    }

    private fun initializePlayer(mediaUri: Uri) {
        if (mExoPlayer == null) {
            //Create an instance of the ExoPlayer
            val trackSelector = DefaultTrackSelector()
            val loadControl = DefaultLoadControl()
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl)
            mPlayerView!!.player = mExoPlayer
            //Prepare the MediaSource
            val userAgent = Util.getUserAgent(this, "My ExoPlayer")
            val mediaSource = ExtractorMediaSource(mediaUri, DefaultDataSourceFactory(
                this, userAgent), DefaultExtractorsFactory(), null, null)
            mExoPlayer!!.prepare(mediaSource)
            mExoPlayer!!.playWhenReady = true
            mExoPlayer!!.addListener(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
        mMediaSession!!.isActive = false
    }

    private fun releasePlayer() {
        mNotificationManager!!.cancelAll()
        mExoPlayer!!.stop()
        mExoPlayer!!.release()
        mExoPlayer = null
    }

    override fun onTimelineChanged(timeline: Timeline, manifest: Any?) {

    }

    override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {

    }

    override fun onLoadingChanged(isLoading: Boolean) {

    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if ((playbackState == ExoPlayer.STATE_READY) && playWhenReady) {
            mStateBuilder!!.setState(PlaybackStateCompat.STATE_PLAYING,
                mExoPlayer!!.currentPosition, 1f)
            Log.d("onPlayerStateChanged:", "PLAYING")
        } else if ((playbackState == ExoPlayer.STATE_READY)) {
            mStateBuilder!!.setState(PlaybackStateCompat.STATE_PAUSED,
                mExoPlayer!!.currentPosition, 1f)
            Log.d("onPlayerStateChanged:", "PAUSED")
        }
        mMediaSession!!.setPlaybackState(mStateBuilder!!.build())
        showNotification(mStateBuilder!!.build())
    }

    override fun onRepeatModeChanged(repeatMode: Int) {

    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

    }

    override fun onPlayerError(error: ExoPlaybackException) {

    }

    override fun onPositionDiscontinuity(reason: Int) {

    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {

    }

    override fun onSeekProcessed() {

    }

    /**
     * Media Session Callbacks, where all external clients control the player.
     */

    private inner class MySessionCallback : MediaSessionCompat.Callback() {
        override fun onPlay() {
            mExoPlayer!!.playWhenReady = true
        }

        override fun onPause() {
            mExoPlayer!!.playWhenReady = false
        }

        override fun onSkipToPrevious() {
            mExoPlayer!!.seekTo(0)
        }
    }

    private fun showNotification(state: PlaybackStateCompat) {

        // You only need to create the channel on API 26+ devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)

        val icon: Int
        val play_pause: String
        if (state.state == PlaybackStateCompat.STATE_PLAYING) {
            icon = R.drawable.exo_controls_pause
            play_pause = "Пауза"
        } else {
            icon = R.drawable.exo_controls_play
            play_pause = "Плей"
        }

        val playPauseAction = NotificationCompat.Action(
            icon, play_pause,
            MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE))

        /* val restartAction = NotificationCompat.Action(
             R.drawable.exo_controls_previous, getString(R.string.restart),
             MediaButtonReceiver.buildMediaButtonPendingIntent(this,
                 PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS))*/

        val contentPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, PlayerActivity::class.java), 0)

        token = mMediaSession!!.sessionToken

        builder.setContentTitle("Радио")
            .setContentText("Трезвое радио")
            .setContentIntent(contentPendingIntent)
            .setSmallIcon(R.drawable.ic_favorite_black_24dp)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .addAction(playPauseAction)
            .setStyle(MediaStyle()
                .setMediaSession(token)
                .setShowActionsInCompactView(0))

        mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager!!.notify(0, builder.build())
    }

    /**
     * Broadcast Receiver registered to receive the MEDIA_BUTTON intent coming from clients
     */

    class MediaReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            MediaButtonReceiver.handleIntent(mMediaSession, intent)
        }
    }

    /**
     * The NotificationCompat class does not create a channel for you. You still have to create a channel yourself
     */

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val mNotificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // The id of the channel.
        val id = CHANNEL_ID
        // The user-visible name of the channel.
        val name = "Media playback"
        // The user-visible description of the channel.
        val description = "Media playback controls"
        val importance = NotificationManager.IMPORTANCE_LOW
        val mChannel = NotificationChannel(id, name, importance)
        // Configure the notification channel.
        mChannel.description = description
        mChannel.setShowBadge(false)
        mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        mNotificationManager.createNotificationChannel(mChannel)
    }

    companion object {

        private val CHANNEL_ID = "media_playback_channel"
        private var mMediaSession: MediaSessionCompat? = null
    }


}