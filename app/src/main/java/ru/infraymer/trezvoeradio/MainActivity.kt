package ru.infraymer.trezvoeradio

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_about.*
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.share
import org.jetbrains.anko.uiThread
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    /***********************************************************************************************
     * Properties
     ***********************************************************************************************/
    private var mPlay = false
    private var mAbout = false
    private lateinit var mClient: OkHttpClient
    private var radioServiceBinder: RadioService.RadioServiceBinder? = null
    private var mediaControllerCompat: MediaControllerCompat? = null


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (intent?.action == "close") finishAffinity()
    }

    /***********************************************************************************************
     * Override funs
     ***********************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        mPLayerManager = PlayerManager(this)
        mClient = OkHttpClient()

        bindService(Intent(this, RadioService::class.java), object : ServiceConnection {
            override fun onServiceDisconnected(p0: ComponentName?) {
                radioServiceBinder = null
                mediaControllerCompat = null
            }

            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                radioServiceBinder = service as RadioService.RadioServiceBinder
                try {
                    mediaControllerCompat = MediaControllerCompat(this@MainActivity, radioServiceBinder?.getMediaSessionToken()!!)
                    mediaControllerCompat?.registerCallback(object : MediaControllerCompat.Callback() {
                        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
                            if (state == null) return
                            mPlay = state.state == PlaybackStateCompat.STATE_PLAYING
                            swapIcon(mPlay)
                        }
                    })
                } catch (e: Exception) {
                    mediaControllerCompat = null
                }
            }

        }, Context.BIND_AUTO_CREATE)

        main_action_btn.setOnClickListener {
            mPlay = !mPlay
            /*if (mPlay)
                mediaControllerCompat?.transportControls?.play()
            else
                mediaControllerCompat?.transportControls?.stop()*/

            if (mPlay)
                radioServiceBinder?.play()
            else
                radioServiceBinder?.stop()

            swapIcon(mPlay)
            runTaskGettingMetaData()
        }
        main_share_btn.setOnClickListener { share() }
        main_info_btn.setOnClickListener { showAbout(true) }
        about_back_btn.setOnClickListener { showAbout(false) }


    }


    override fun onBackPressed() {
        if (mAbout) showAbout(false)
        else super.onBackPressed()
    }

    /***********************************************************************************************
     * Private funs
     ***********************************************************************************************/
    private fun swapIcon(play: Boolean) {
        main_pause_image_view.visible(play)
        main_play_image_view.visible(!play)
    }

    private fun showAbout(show: Boolean) {
        mAbout = show
        if (show) {
            layout_about.alpha = 0f
            layout_about.visible()
            layout_about.animate()
                .setDuration(300)
                .alphaBy(0f)
                .alpha(1f)
                .start()
        } else {
            layout_about.animate()
                .setDuration(300)
                .alphaBy(1f)
                .alpha(0f)
                .withEndAction { layout_about.gone() }
                .start()
        }
    }

    private fun share() {
        share("Будь трезвым, слушай трезвое радио!")
    }

    private fun getMetaRadio(): String {
        return try {
            val request = Request.Builder()
                .url("http://eu3.radioboss.fm:8259/currentsong?sid=1")
                .build()
            val response = mClient.newCall(request).execute()
            val s = response.body()?.string() ?: ""
            val result = String(s.toByteArray(Charsets.ISO_8859_1), Charset.forName("windows-1251"))
            result
        } catch (e: Exception) {
            e.printStackTrace()
            runTaskGettingMetaData().cancel(true)
            ""
        }
    }

    private fun runTaskGettingMetaData() = doAsync {
        while (mPlay) {
            Log.d(javaClass.simpleName, "Getting meta data")
            val data = getMetaRadio()
            uiThread { main_radio_meta_tv.text = data }
            TimeUnit.SECONDS.sleep(5)
        }
        Log.d(javaClass.simpleName, "Finished thread")
    }
}
