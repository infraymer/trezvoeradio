@file:Suppress("NOTHING_TO_INLINE")

package ru.infraymer.trezvoeradio

import android.view.View

/**
 * Created by infraymer on 29.11.17.
 */
inline fun View.visible(show: Boolean = true) {
    visibility = if (show) View.VISIBLE else View.GONE
}

inline fun View.invisible() {
    visibility = View.INVISIBLE
}

inline fun View.gone() {
    visibility = View.GONE
}